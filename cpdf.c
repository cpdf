/*
 * ctxt - A small and simple pdf viewer using gtk+ and poppler
 *
 * Copyright (C) 2009 Ali Gholami Rudi
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License, as published by the
 * Free Software Foundation.
 *
 */
#include <ctype.h>
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>
#include <glib/poppler.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static GtkWidget *bar;
static GtkWidget *img;
static GtkAdjustment *vadjst;
static GtkAdjustment *hadjst;
static PopplerDocument *doc;
static PopplerPage *page;
static GdkPixbuf *pixbuf;
static char filename[PATH_MAX];
static int num;
static int zoom = 15;
static int arg;

enum location {
	LOC_PAGE,
	LOC_STEP,
	LOC_BEG,
	LOC_CENTER,
	LOC_END
};

static void on_destroy(GtkWidget *w, gpointer data)
{
	gtk_main_quit();
}

static void drawbar()
{
	int val = gtk_adjustment_get_value(vadjst) +
		  gtk_adjustment_get_page_size(vadjst);
	int max = gtk_adjustment_get_upper(vadjst);
	int pages = poppler_document_get_n_pages(doc);
	char msg[128];
	sprintf(msg, "%s\t%d%%\t\t\t%d/%d",
		filename, val * 100 / max, num + 1, pages);
	gtk_entry_set_text(GTK_ENTRY(bar), msg);
}

static void redraw()
{
	double w = 0, h = 0;
	double z = (float) zoom / (float) 10;
	if (!page)
		return;
	poppler_page_get_size(page, &w, &h);
	w *= z;
	h *= z;
	if (pixbuf)
		g_object_unref(G_OBJECT(pixbuf));
	pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, w, h);
	poppler_page_render_to_pixbuf(page, 0, 0, w, h, z, 0, pixbuf);
	gtk_image_set_from_pixbuf(GTK_IMAGE(img), pixbuf);
	drawbar();
}

static void cleanup()
{
	if (pixbuf)
		g_object_unref(G_OBJECT(pixbuf));
	if (page)
		g_object_unref(G_OBJECT(page));
	if (doc)
		g_object_unref(G_OBJECT(doc));
}

static int get_arg(int def)
{
	return !arg ? def : arg;
}

static void scroll(enum location loc, int n)
{
	int val = gtk_adjustment_get_value(vadjst);
	int page = gtk_adjustment_get_page_size(vadjst);
	int end = gtk_adjustment_get_upper(vadjst);
	int max = end - page;
	n *= get_arg(1);
	switch (loc) {
	case LOC_PAGE:
		val += n * gtk_adjustment_get_page_increment(vadjst);
		break;
	case LOC_STEP:
		val += n * gtk_adjustment_get_step_increment(vadjst);
		break;
	case LOC_BEG:
		val = 0;
		break;
	case LOC_CENTER:
		val = (end - page) / 2;
		break;
	case LOC_END:
		val = max;
		break;
	}
	arg = 0;
	gtk_adjustment_set_value(vadjst, MAX(0, MIN(max, val)));
	drawbar();
}

static void hscroll(int n)
{
	int val = gtk_adjustment_get_value(hadjst);
	int page = gtk_adjustment_get_page_size(hadjst);
	int end = gtk_adjustment_get_upper(hadjst);
	int max = end - page;
	arg = 0;
	val += n * get_arg(1) * gtk_adjustment_get_step_increment(hadjst);
	gtk_adjustment_set_value(hadjst, MAX(0, MIN(max, val)));
}

static void showpage(int p)
{
	if (p < 0 || p >= poppler_document_get_n_pages(doc))
		return;
	num = p;
	if (page)
		g_object_unref(G_OBJECT(page));
	page = poppler_document_get_page(doc, p);
	redraw();
	gtk_adjustment_set_value(vadjst, 0);
}

static void nextpage(int n)
{
	showpage(num + n * get_arg(1));
	arg = 0;
}

static gboolean on_key_press(GtkWidget *widget, GdkEventKey *event,
			     gpointer data)
{
	int mod = event->state & gtk_accelerator_get_default_mod_mask();
	if (mod == GDK_CONTROL_MASK) {
		switch (event->keyval) {
		case GDK_f:
			nextpage(1);
			break;
		case GDK_b:
			nextpage(-1);
			break;
		case GDK_l:
			gtk_widget_queue_draw(img);
			break;
		default:
			break;
		}
		return 1;
	}
	switch (event->keyval) {
	case GDK_Down:
	case GDK_j:
		scroll(LOC_STEP, 1);
		break;
	case GDK_Up:
	case GDK_k:
		scroll(LOC_STEP, -1);
		break;
	case GDK_Left:
	case GDK_h:
		hscroll(-1);
		break;
	case GDK_Right:
	case GDK_l:
		hscroll(1);
		break;
	case GDK_G:
		showpage(get_arg(poppler_document_get_n_pages(doc)) - 1);
		arg = 0;
		break;
	case GDK_End:
	case GDK_L:
		scroll(LOC_END, 1);
		break;
	case GDK_Home:
	case GDK_H:
		scroll(LOC_BEG, 1);
		break;
	case GDK_M:
		scroll(LOC_CENTER, 1);
		break;
	case GDK_Next:
	case GDK_space:
		scroll(LOC_PAGE, 1);
		break;
	case GDK_Prior:
	case GDK_BackSpace:
		scroll(LOC_PAGE, -1);
		break;
	case GDK_q:
		gtk_main_quit();
		break;
	case GDK_z:
		if (get_arg(0) > 5 && get_arg(0) < 50)
			zoom = get_arg(0);
		arg = 0;
		redraw();
		break;
	case GDK_Escape:
		arg = 0;
		break;
	default:
		if (isdigit(event->keyval))
			arg = arg * 10 + event->keyval - '0';
		break;
	}
	return 1;
}

static void on_size_allocate(GtkWidget *w, GtkAllocation *a,
			     gpointer data)
{
	drawbar();
}

static int load_document(void)
{
	char abspath[PATH_MAX];
	char uri[PATH_MAX + 16];
	realpath(filename, abspath);
	snprintf(uri, sizeof(uri), "file://%s", abspath);
	doc = poppler_document_new_from_file(uri, NULL, NULL);
	return !doc;
}

static void create_window(void)
{
	GtkWidget *win;
	GtkWidget *wrap;
	GtkWidget *vbox;

	win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(win), "mypdf");
	g_signal_connect(G_OBJECT(win), "destroy",
			 G_CALLBACK(on_destroy), NULL);

	vbox = gtk_vbox_new(FALSE, 2);
	gtk_container_add(GTK_CONTAINER(win), vbox);

	/* create the image */
	wrap = gtk_scrolled_window_new(NULL, NULL);
	gtk_box_pack_start(GTK_BOX(vbox), wrap, 1, 1, 0);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(wrap),
					GTK_POLICY_NEVER,
					GTK_POLICY_NEVER);
	vadjst = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(wrap));
	hadjst = gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(wrap));
	img = gtk_image_new();
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(wrap), img);

	/* create the minibuffer */
	bar = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(vbox), bar, 0, 0, 0);
	gtk_signal_connect(GTK_OBJECT(bar), "key_press_event",
			   GTK_SIGNAL_FUNC(on_key_press), NULL);
	gtk_signal_connect(GTK_OBJECT(win), "key_press_event",
			   GTK_SIGNAL_FUNC(on_key_press), NULL);
	gtk_signal_connect(GTK_OBJECT(win), "size_allocate",
			   GTK_SIGNAL_FUNC(on_size_allocate), NULL);

	gtk_widget_set_size_request(win, 800, 600);
	gtk_window_set_default_size(GTK_WINDOW(win), 1024, 768);
	gtk_widget_show_all(win);
}

static int read_args(int argc, char *argv[])
{
	int i = 0;
	while (++i < argc) {
		if (!strcmp("-z", argv[i]))
			zoom = atoi(argv[++i]);
		else if (!strcmp("-p", argv[i]))
			num = atoi(argv[++i]) - 1;
		else
			strcpy(filename, argv[i]);
	}
	return !*filename;
}

int main(int argc, char *argv[])
{
	gtk_init(&argc, &argv);
	if (read_args(argc, argv) || load_document()) {
		puts("Usage: cpdf [-p page] [-z zoom] path\n");
		return 1;
	}
	create_window();
	showpage(num);
	gtk_main();
	cleanup();
	return 0;
}
