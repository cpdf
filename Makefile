CC = cc
CFLAGS = -std=gnu89 -pedantic -Wall -O2 \
	`pkg-config --cflags gtk+-2.0 poppler-glib`
LDFLAGS = -s `pkg-config --libs gtk+-2.0 poppler-glib`

all: cpdf
.c.o:
	$(CC) -c $(CFLAGS) $<
cpdf: cpdf.o
	$(CC) $(LDFLAGS) -o $@ $^
clean:
	rm -f *.o cpdf
ctags:
	ctags *.[hc]
